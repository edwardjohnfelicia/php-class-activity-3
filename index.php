<?php require './code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<title>PHP OOP | Activity</title>
</head>
<body>
	<pre>
		<?php print_r($newProduct); ?>
	</pre>
	<pre>
		<?php print_r($newComputer); ?>
	</pre>
	<pre>
		<?php print_r($newMobile); ?>
	</pre>

	<ul>
		<li>
			Product: <?php echo $newProduct->getName(); ?>
		</li>
		<li>
			Product: <?php echo $newComputer->getName(); ?>
		</li>
		<li>
			Product: <?php echo $newMobile->getName(); ?>
		</li>
		<?php 
			$newProduct->setStockNo(3);
			$newComputer->setStockNo(5);
			$newMobile->setCategory('laptops, computers and electronics');
		 ?>
	</ul>

	<pre>
		<?php print_r($newProduct); ?>
	</pre>
	<pre>
		<?php print_r($newComputer); ?>
	</pre>
	<pre>
		<?php print_r($newMobile); ?>
	</pre>
	
</body>
</html>